const express = require('express')
const router = express.Router()
const ControllerHome = require('../controllers/home')



//sesssion login
/*
router.use((req, res, next) => {
    if('user' in req.session){
        return next()
    }
    res.redirect('/login')
})
router.use((req,res, next) =>{
    if('user' in req.session){
        res.locals.user = req.session.user    
    }
    next()

})*/


//passport login in session
router.use((req, res, next) => {
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
})

router.use((req, res, next) => {
    if(req.isAuthenticated()){
        res.locals.user = req.user
    }
    next()
})

router.get('/',ControllerHome.index)


module.exports = router