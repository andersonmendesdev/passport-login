const bcrypt = require('bcrypt')

const index = async(req, res) => {
    if(req.user){
       return res.redirect('/')
    }
    res.render('login', {error: false} )
}
const loginUser = async({ User }, req, res) =>{
    const { email , password } = req.body
    const user = await User.findOne({ email: email}).select('+password')
    
    if(!user){
        return res.render('login', {error: true})
    }

    if(!await bcrypt.compare(password, user.password)){
        return res.render('login', {error: true})
    }
    user.password = undefined
    req.session.user = user
    res.redirect('/')
}
const logoutUser = async(req, res) => {
    req.session.destroy(() => {
        res.redirect('/login')
    })
}

module.exports = {
    loginUser, index, logoutUser
}