//const mongoose = require('mongoose')
const mongoose = require('../../database/index')
const brcypt = require('bcrypt')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    passwordResetToken: {
        type: String,
        select: false
    },
    passwordResetExpires: {
        type: Date,
        select: false
    },
    facebookId: {
        type: String
    },
    googleId: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

userSchema.pre('save', function(next){
    const user = this

    if(!user.isModified('password')){
        return next()
    }

    brcypt.genSalt((err, salt) => {
        if(err){
            console.log(err)
        }else{
            brcypt.hash(user.password, salt, (err, hash) => {
                if(err){
                    console.log(err)
                }else{
                    user.password = hash
                    next()
                }                
            })
        }        
    })
})

const User = mongoose.model('User', userSchema)
module.exports = User