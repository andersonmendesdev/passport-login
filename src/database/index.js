const mongoose = require('mongoose')
//onst mongo = process.env.MONGODB || 'mongodb://localhost/PassportUser'
const mongo = process.env.MONGOURL || 'mongodb://localhost:27017/PassportUser'

try{
    mongoose.connect(mongo, { useNewUrlParser: true })
    mongoose.Promise = global.Promise

}catch(e){
    console.log(e)
}


module.exports = mongoose