const express = require('express')
const path = require('path')
const app = express()
const http = require('http').Server(app)
const bodyParser = require('body-parser')
const session = require('express-session')
const homeRouter = require('./src/app/routes/home')
const loginRouter = require('./src/app/routes/auth')
const port = process.env.PORT || 8081


//body
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true}))

//session
app.use(session({ 
    secret: 'andersonmendesdev',
    resave: false,
    saveUninitialized: true 
}))

//view
app.set('views', path.join(__dirname,'./src/app/views'))
app.set('view engine', 'ejs')

//folder public
app.use(express.static('./src/public'))

//router
app.use('/', loginRouter)
app.use('/', homeRouter)



http.listen(port, () => console.log('running in port '+port))


